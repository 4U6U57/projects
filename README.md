# Projects

Ideas for side projects I come up with. View on [GitLab issues][issues].

## Contributing

Input and feedback on my ideas are always welcome, feel free to comment on the
issue in question and I'll try to reply back and edit the description if I think
it's worth pursuing. Also, feel free to steal these ideas and make your own
version of them.

## Proposal Format

All project proposals should follow a standard format. When creating the issue,
use the template ["Proposal"][template].

## Repo

*Please do not commit or request changes to this repo (aside from corrections to
this documentation). All project proposals should be logged as [issues][issues].*

[issues]: https://gitlab.com/4U6U57/projects/issues
[template]: .gitlab/issue_templates/Proposal.md
