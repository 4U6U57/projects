# Summary

> A brief 1-2 sentence summary of the project.

# Problem Statement

> The rationale behind this project from a user perspective.

# Proposal

> Actual information on the implementation, should be an expanded form of
> "Summary".

# References

- A list of useful links,
- existing alternatives,
- or inspiration for the project.

# Timeline

- [ ] Project tasks, first should be:
- [x] Proposed
